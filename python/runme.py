import zfit
from zfit import z

###############################################################################
#                                                                             #
# Building the PDF to fit to the data                                         #
#                                                                             #
###############################################################################

#define the variable in which we fit
mass = zfit.Space('B_M', (4900, 7000))

#define the model parameters, for the signal we've got a gaussian, so need a
#mean and sigma, and a yield
mean  = zfit.Parameter('mean', 5250, 4900, 6000)
sigma = zfit.Parameter('sigma', 40,    5,  100)
signalYield = zfit.Parameter('signalYield', 1000, 0, 20000)

#define the signal model
signalPDF = zfit.pdf.Gauss(obs=mass, mu=mean, sigma=sigma)
signalPDF.set_yield(signalYield)


#We're using an exponential for the background model, so we have an exponent
#parameter (and a yield)
exponent  = zfit.Parameter('exponent', -0.01, -1e-1, -1e-6)
backgroundYield = zfit.Parameter('backgroundYield', 1000, 0, 20000)

#define the background model
backgroundPDF = zfit.pdf.Exponential(obs=mass, lam=exponent)
backgroundPDF.set_yield(backgroundYield)

#The total pdf to fit to the data is the sum of the signal and background
totalPDF = zfit.pdf.SumPDF(pdfs=[signalPDF, backgroundPDF])


###############################################################################
#                                                                             #
# Get the data to fit to                                                      #
#                                                                             #
###############################################################################
#here we extract the data from a ROOT tuple, the branch name is the same as
#the mass variable name above, and we're not applying any cuts to the data

data = zfit.Data.from_root('data/example.root', 'DataTuple', mass)


###############################################################################
#                                                                             #
# Run the fit to the data                                                     #
#                                                                             #
###############################################################################
#We want to make an extended unbinned likelihood fit, so we first make the
#likelihood from the model and data
nll = zfit.loss.ExtendedUnbinnedNLL(model=totalPDF, data=data)

#We then ask the minimiser (Minuit here) find the parameters that minimise the
#liklehood
minimizer = zfit.minimize.Minuit()
result = minimizer.minimize(nll)

#To compute the uncertainty on the parameters we can use Hesse. This estimates
#the uncertainties from the second gradient at the minimum. This is good when
#the likelihood is nicely parabolic, i.e. in the asymptotic limit. The coverage
#of Hesse should always be checked. There are alternatives, e.g. Minos that
#can cope with slightly asymmetric minima, but again the coverage should always
#be checked.
param_hesse = result.hesse()

#print the result, and also explicitly access the result information. This is
#important as the minimiser will always complete, but that doesn't mean it has
#converged or that it has found a sensible minimum. Minuit has a few parameters
#to tweak how it behaves, the strategy and tolerance are usually the most
#important, but sometimes it is necessary to kick the minimiser out of e.g. a
#local minimum.
print(result)
print(f"Function minimum: {result.fmin}")
print(f"Converged: {result.converged}")
print(f"Valid: {result.valid}")

###############################################################################
#                                                                             #
# Plotting the fit result                                                     #
#                                                                             #
###############################################################################

#here we will use matplotlib to plot the data, then the total PDF, and the
#individual components. It is a little convoluted to normalise the PDFs
#correctly, there is probably a simpler way

import mplhep
import matplotlib.pyplot as plt
import numpy as np

plt.rc('text', usetex=True)
plt.rcParams['savefig.dpi'] = 100
plt.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
plt.figure(figsize=(5.6,4.2))

# plot the data as a histogram with a specified number of bins
bins = 100
mplhep.histplot(data.to_binned(bins), yerr=True, xerr=True, density=False, color='black', histtype='errorbar',markersize=2)

# evaluate the pdf at multiple x positions and plot. The PDF is normalised, so
#the plot must be scaled by the yield, and as we're comparing to histogram,
#the bin width.
binwidth = (mass.upper[0]-mass.lower[0])/bins
x_plot = np.linspace(mass.lower[0], mass.upper[0], num=1000)
tot_plot = (signalYield+backgroundYield)*binwidth*totalPDF.pdf(x_plot)
plt.plot(x_plot, tot_plot, color='xkcd:blue', label='Total')
sig_plot = signalYield*binwidth*signalPDF.pdf(x_plot)
plt.plot(x_plot, sig_plot, color='xkcd:red', label=r'$B^\pm\to J/\psi K^\pm$')
bkg_plot = backgroundYield*binwidth*backgroundPDF.pdf(x_plot)
plt.plot(x_plot, bkg_plot, color='xkcd:green', label='Combinatorial')
plt.xlabel('$m(K^\pm\mu^+\mu^-)$ [GeV]')
plt.ylabel('Candidates')
plt.legend()
plt.savefig('out.pdf')

