

#This sets up a nice environment on the lxplus-like machines
if [ -f /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh ]; then
   echo "Running initial config script, this can take some time even though it shouldn't"
   source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh
fi

export QT_QPA_PLATFORM=offscreen
export PYTHONPATH="$(pwd)/python/:${PYTHONPATH}"

